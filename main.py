import sqlite3

from flask import Flask, g, json, abort

app = Flask(__name__)

DATABASE_PATH = "example.db"

def get_db():
    db = getattr(g, "_database", None)
    if db is None:
        db = g._database = sqlite3.connect(DATABASE_PATH)
    return db

@app.teardown_appcontext
def close_db(exception):
    db = getattr(g, '_database', None)
    if db is not None:
        db.close()

@app.route("/hw")
def helloworld():
    return "Hello World!"

@app.route("/all")
def get_all():
    con = get_db()
    cur = con.cursor()

    thing = []
    for data in cur.execute("SELECT * FROM stocks"):
        thing.append({"date": data[0],
                    "transaction": data[1],
                    "symbol": data[2],
                    "qty": data[3],
                    "price": data[4]
    })

    con.commit()
    return json.jsonify(thing)

@app.route("/by/<key>/<value>")
def get_by_arbitrary(key, value):
    con = get_db()
    cur = con.cursor()

    key = key.lower()

    if key == "date":
        cur.execute('SELECT * FROM stocks WHERE date=?;', (value,))
    elif key == "trans":
        cur.execute('SELECT * FROM stocks WHERE trans=?;', (value,))
    elif key == "symbol":
        cur.execute('SELECT * FROM stocks WHERE symbol=?;', (value,))
    elif key == "qty":
        cur.execute('SELECT * FROM stocks WHERE qty=?;', (value,))
    elif key == "price":
        cur.execute('SELECT * FROM stocks WHERE price=?;', (value,))
    else:
        abort(404)

    thing = []
    for data in cur.fetchall():
        print(data)
        thing.append({"date": data[0],
                    "transaction": data[1],
                    "symbol": data[2],
                    "qty": data[3],
                    "price": data[4]
    })

    con.commit()
    return json.jsonify(thing)

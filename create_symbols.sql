DROP TABLE symbols;
CREATE TABLE symbols
             (symbol text, id integer primary key autoincrement);

INSERT INTO symbols VALUES ('RHAT', null);
INSERT INTO symbols VALUES ('NOVL', null);
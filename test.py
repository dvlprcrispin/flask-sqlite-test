import sqlite3

db = sqlite3.connect("example.db")


cur = db.cursor()

# cur.execute('SELECT * FROM stocks WHERE ?=?;', ("trans", "TRADE"))
cur.execute('SELECT * FROM stocks WHERE trans=?;', ("TRADE",))

print(cur.fetchall())

db.commit()

db.close()
DROP TABLE STOCKS;
CREATE TABLE `stocks` (
	`date`	TEXT,
	`trans`	INTEGER,
	`symbol` INTEGER,
	`qty`	REAL,
	`price`	REAL,
	`id`	INTEGER PRIMARY KEY AUTOINCREMENT,
	FOREIGN KEY(`trans`) REFERENCES `trans_types`(`id`)
	FOREIGN KEY(`symbol`) REFERENCES `symbols`(`id`)
);

INSERT INTO stocks VALUES ('2006-01-05', (SELECT id FROM trans_types WHERE type='BUY'), (SELECT id FROM symbols where symbol='RHAT'), 100, 35.14, null);
INSERT INTO stocks VALUES ('2008-01-05', (SELECT id FROM trans_types WHERE type='SELL'), (SELECT id FROM symbols where symbol='RHAT'), 100, 35.14, null);
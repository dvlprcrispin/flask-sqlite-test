DROP TABLE people;
CREATE TABLE people
             (person text, id integer primary key autoincrement);

INSERT INTO people VALUES ('Gates', null);
INSERT INTO people VALUES ('Ellison', null);
INSERT INTO people VALUES ('Linus', null);
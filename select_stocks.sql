SELECT stocks.date, trans_types.type, symbols.symbol, stocks.qty, stocks.price, stocks.id FROM stocks 
JOIN symbols ON stocks.symbol = symbols.id 
JOIN trans_types ON stocks.trans = trans_types.id;